package edu.wgu.tgerlac.library.model;

@SuppressWarnings("serial")
public class PatronData extends PersonData implements Comparable<PatronData> {
	
	private int id;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private boolean active = true;
	private String barcode;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PatronData other = (PatronData) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PatronData [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + "]";
	}

	@Override
	public int compareTo(PatronData o) {
		if (this.lastName.equals(o.lastName)) {
			return this.firstName.compareTo(o.firstName);
		}
		return lastName.compareTo(o.lastName); 
	}
}
