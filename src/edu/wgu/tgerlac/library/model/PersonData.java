package edu.wgu.tgerlac.library.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class PersonData implements Serializable {

	protected String lastName;
	protected String firstName;
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFirstLast() {
		return firstName + " " + lastName;
	}
	
	public String getLastFirst() {
		return lastName + ", " + firstName;
	}
}
