package edu.wgu.tgerlac.library.model;

import java.io.Serializable;

public class CopyData implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private int bookId;
	private String barcode;
	
	public CopyData() {
		super();
	}
	
	public CopyData(int bookId) {
		super();
		this.bookId = bookId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CopyData other = (CopyData) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CopyData [barcode=" + barcode + "]";
	}
}
