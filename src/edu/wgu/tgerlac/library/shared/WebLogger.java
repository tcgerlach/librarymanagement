package edu.wgu.tgerlac.library.shared;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.wgu.tgerlac.library.model.UserData;

public class WebLogger {
	private static Logger logger = LogManager.getLogger(LibraryConstants.LOGGER);
	
	/**
	 * Add an info entry to the log.
	 * 
	 * @param user generating the message
	 * @param message to be displayed
	 */
	public static void info(final UserData user, final String message) {
		logger.info(user.getUserId() + " - " + message);
	}
	
	/**
	 * Add an info entry to the log.
	 * 
	 * @param message to be displayed
	 */
	public static void info(final String message) {
		logger.info(message);
	}
	
	/**
	 * Add a debug entry to the log.
	 * 
	 * @param user generating the message
	 * @param message to be displayed
	 */
	public static void debug(final UserData user, final String message) {
		logger.info(user.getUserId() + " - " + message);
	}
	
	/**
	 * Add a debug entry to the log.
	 * 
	 * @param message to be displayed
	 */
	public static void debug(final String message) {
		logger.info(message);
	}
	
	/**
	 * Add an error entry to the log.
	 * 
	 * @param user generating the message
	 * @param message to be displayed
	 */
	public static void error(final String message) {
		logger.error(message);
	}
	
	/**
	 * Add an error entry to the log.
	 * 
	 * @param message to be displayed
	 */
	public static void error(final String message, final Throwable e) {
		logger.error(message, e);
	}
}
