package edu.wgu.tgerlac.library.shared;

import java.beans.Introspector;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;

public class BeanHelper {
	
	/**
	 * Retrieve an instance of the bean specified by clazz.
	 * 
	 * @param clazz Bean class to retrieve
	 * @return instance of the bean
	 */
	public static <T extends Object> T getBean(Class<T> clazz) {
		FacesContext context = FacesContext.getCurrentInstance();
		Application app = context.getApplication();
		String beanName = "#{" + Introspector.decapitalize(clazz.getSimpleName()) + "}";
		return app.evaluateExpressionGet(context, beanName, clazz);
	}
}
