package edu.wgu.tgerlac.library.shared;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.faces.context.FacesContext;

/**
 * Interface for library properties file.
 * 
 * @author tcgerlach
 */
public class LibraryProperties {

	private static Properties properties;	

	static {		
		WebLogger.debug("Loading library.properties...");
		
		FileInputStream fis = null;
		
		try {
			String confDir = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF" + File.separator;
			WebLogger.debug("Conf dir is " + confDir);
			
			properties = new Properties();
			fis = new FileInputStream(confDir + "library.properties");
			properties.load(fis);					
		} catch (FileNotFoundException e) {
			WebLogger.error("Properties file not found", e);
		} catch (IOException e) {
			WebLogger.error("Error loading properties file", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}
	}
	
	/**
	 * Retrieve a property from the properties file.
	 * 
	 * @param name of the desired property
	 * @return value from the properties file
	 */
	public static Object getProperty(final String name) {
		return properties.get(name);
	}
	
	/**
	 * Retrieve a property from the properties file. If the property
	 * does not exist, return the default value provided.
	 * 
	 * @param name of the desired property
	 * @param defaultValue if the property does not exist
	 * @return value from the properties file
	 */
	public static Object getProperty(final String name, final Object defaultValue) {
		return properties.get(name) == null ? defaultValue : properties.get(name);
	}
}