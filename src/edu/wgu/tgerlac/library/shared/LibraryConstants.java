package edu.wgu.tgerlac.library.shared;

/**
 * Program-wide constants
 * 
 * @author tcgerlach
 */
public class LibraryConstants {
	public static final String LOGGER = "edu.wgu.tgerlac.library";
	
	public static final long MS_PER_SECOND = 1000;
	public static final long MS_PER_MINUTE = MS_PER_SECOND * 60;
	public static final long MS_PER_HOUR = MS_PER_MINUTE * 60;
	public static final long MS_PER_DAY = MS_PER_HOUR * 60;
}
