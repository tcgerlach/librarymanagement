package edu.wgu.tgerlac.library.shared;

import java.io.File;

import javax.faces.context.FacesContext;

public class FileFinder {
	
	public static String getResourceDirectory() {
		return FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + File.separator + "WEB-INF" + File.separator;
	}
}
