package edu.wgu.tgerlac.library.dao;

import java.util.ArrayList;
import java.util.List;

import edu.wgu.tgerlac.library.dao.database.DatabaseHelper;

public class CheckoutDAO {

	private static final String IS_CHECKEDOUT = 
			"select count(*) from checkouts c inner join copies b on c.copy_id = b.id where indate is null and barcode = ?";
	
	private static final String DUE_COUNT = 
			"select count(*) from checkout_view c where patron_id = ? and indate is null and duedate < Now()";
	
	private static final String CHECKIN = 
			"update checkouts set indate = now() where copy_id = (select id from copies where barcode = ?) and indate is null";
	
	private static final String CHECKOUT =
			"insert into checkouts (copy_id, patron_id) values (?,?)";
	
	private static final String DELETE_CHECKOUTS =
			"delete from checkouts where copy_id = ?";
	
	public static int getPastDueCountForPatron(final int patronId) {
		List<Object> params = new ArrayList<Object>();
        params.add(patronId);
        return DatabaseHelper.getIntResultByQuery(DUE_COUNT, params); 
	}
	
	public static boolean isBookCheckedOut(final String barcode) {
		List<Object> params = new ArrayList<Object>();
        params.add(barcode);
        return DatabaseHelper.getIntResultByQuery(IS_CHECKEDOUT, params) > 0; 
	}
	
	public static void checkinBook(final String barcode) {
		List<Object> params = new ArrayList<Object>();
		params.add(barcode);
		DatabaseHelper.writeData(CHECKIN, params, "Error checking book in");
	}
	
	public static void checkoutBook(final int copyId, final int patronId) {
		List<Object> params = new ArrayList<Object>();
		params.add(copyId);
		params.add(patronId);
		DatabaseHelper.writeData(CHECKOUT, params, "Error checking book out");
	}
	
	public static void deleteCheckouts(final int copyId) {
		List<Object> params = new ArrayList<Object>();
		params.add(copyId);
		DatabaseHelper.writeData(DELETE_CHECKOUTS, params, "Error deleting copy");
	}
}
