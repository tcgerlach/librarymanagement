package edu.wgu.tgerlac.library.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.wgu.tgerlac.library.dao.database.DataInserter;
import edu.wgu.tgerlac.library.dao.database.DataLoader;
import edu.wgu.tgerlac.library.dao.database.DatabaseHelper;
import edu.wgu.tgerlac.library.dao.database.interfaces.DataObjectReader;
import edu.wgu.tgerlac.library.dao.database.interfaces.ResultSetReader;
import edu.wgu.tgerlac.library.model.CopyData;

public class CopyDAO implements ResultSetReader<CopyData>, DataObjectReader<CopyData> {
	
	public static final String TABLE_NAME = "COPIES";
	
	// define clauses
	private static final String SELECT_CLAUSE = "select * ";
	private static final String FROM_CLAUSE = "from copies ";

	// define queries
	private static final String LOAD_BY_ID = SELECT_CLAUSE + FROM_CLAUSE + "where id = ?";
	private static final String LOAD_BY_BARCODE = SELECT_CLAUSE + FROM_CLAUSE  + "where barcode = ?";
	private static final String LOAD_BY_BOOK = SELECT_CLAUSE + FROM_CLAUSE + "where book_id = ?";
	
	private static final String INSERT_COPY = "INSERT INTO copies (book_id, barcode) VALUES(?,?)";
	private static final String UPDATE_COPY = "UPDATE copies set book_id = ?, barcode = ? where id = ?";
	
	private static final String DELETE_COPY = "DELETE from copies where id = ?";

	// create interface objects
	private static ResultSetReader<CopyData> rsReader;
	private static DataObjectReader<CopyData> doReader;
	
	static {
		CopyDAO dao = new CopyDAO();
		rsReader = dao;
		doReader = dao;
	}
	
	public static CopyData getCopyById(final int id) {
		List<Object> params = new ArrayList<Object>();
        params.add(id);
        return DataLoader.loadRecordFromResultSet(LOAD_BY_ID, params, rsReader); 
	}
	
	public static CopyData getCopyByBarcode(final String barcode) {
		List<Object> params = new ArrayList<Object>();
        params.add(barcode);
        return DataLoader.loadRecordFromResultSet(LOAD_BY_BARCODE, params, rsReader); 
	}
	
	public static List<CopyData> getCopiesByBook(final int bookId) {
		List<Object> params = new ArrayList<Object>();
        params.add(bookId);
        return DataLoader.loadListFromResultSet(LOAD_BY_BOOK, params, rsReader); 
	}
	
	public static int insertCopy(final CopyData copy) {
		return DataInserter.insertRecord(INSERT_COPY, doReader, copy);
	}
	
	public static void updateCopy(final CopyData copy) {
		DataInserter.updateRecord(UPDATE_COPY, doReader, copy);
	}
	
	public static void deleteCopy(final int copyId) {
		List<Object> params = new ArrayList<Object>();
		params.add(copyId);
		DatabaseHelper.writeData(DELETE_COPY, params, "Error deleting copy");
	}

	public List<Object> getParamListFromObject(final CopyData copy, final boolean isNew) {
		List<Object> values = new ArrayList<Object>();
				
		values.add(copy.getBookId());
		values.add(copy.getBarcode());
		
		if (!isNew) {
			values.add(copy.getId());
		}
		
        return values;
	}

	public CopyData loadRecordFromResultSet(final ResultSet rs) throws SQLException {
		CopyData copy = new CopyData();
		copy.setId(rs.getInt("id"));
		copy.setBookId(rs.getInt("book_id"));
		copy.setBarcode(rs.getString("barcode"));
		return copy;
	}
}
