package edu.wgu.tgerlac.library.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.wgu.tgerlac.library.dao.database.DataInserter;
import edu.wgu.tgerlac.library.dao.database.DataLoader;
import edu.wgu.tgerlac.library.dao.database.DatabaseHelper;
import edu.wgu.tgerlac.library.dao.database.interfaces.DataObjectReader;
import edu.wgu.tgerlac.library.dao.database.interfaces.ResultSetReader;
import edu.wgu.tgerlac.library.model.UserData;

/**
 * DAO class for user table
 * 
 * @author tcgerlach
 */
public class UserDAO implements ResultSetReader<UserData>, DataObjectReader<UserData> {

	public static final String TABLE_NAME = "USERS";
	
	private static final String PASSWORD_SALT = "lib2019";	// add salt to prevent password tables

	// define clauses
	private static final String SELECT_CLAUSE = "select id, userid, lastname, firstname, enabled, admin ";
	private static final String FROM_CLAUSE = "from users u ";
	private static final String ORDER_BY = "order by lastname, firstname ";
	
	// define queries
	private static final String LOAD_ALL_USERS    = SELECT_CLAUSE + FROM_CLAUSE  + ORDER_BY;
	private static final String LOAD_BY_USER_ID   = SELECT_CLAUSE + FROM_CLAUSE  + "where lower(userid) = ?";
	private static final String LOAD_BY_ID        = SELECT_CLAUSE + FROM_CLAUSE  + "where u.id = ?";
	private static final String LOAD_BY_TOKEN     = SELECT_CLAUSE + FROM_CLAUSE  + "where reset_token = ?";
	private static final String AUTHENTICATE_USER = SELECT_CLAUSE + FROM_CLAUSE  + "where lower(userid) = ? and md5(?) = password";

	private static final String INSERT_USER = 
		"insert into users (userid, lastname, firstname, enabled, admin, password) values " +
		"(?,?,?,?,?,md5(?))";
	
	private static final String UPDATE_USER = 
		"update users set userid = ?, lastname = ?, firstname = ?, enabled = ?, admin = ? where id = ?";
	
	private static final String IS_PASSWORD_SET = "select isnull(password) from users where id = ?";
	private static final String IS_USER_UNIQUE = "select count(*) from users where lower(userid) = ?";
	private static final String IS_USER_UNIQUE2 = "select count(*) from users where lower(userid) = ? and id <> ?";
	
	// create interface objects
	private static ResultSetReader<UserData> rsReader;
	private static DataObjectReader<UserData> doReader;
	static {
		UserDAO dao = new UserDAO();
		rsReader = dao;
		doReader = dao;
	}
		
	/**
	 * Retrieve a list of all users
	 * 
	 * @return user list
	 */
	public static List<UserData> getUsers() {
		return DataLoader.loadListFromResultSet(LOAD_ALL_USERS, null, rsReader);
	}
	
	/**
	 * Retrieve a user by their id (primary key)
	 * 
	 * @param id number of user to retrieve
	 * @return user record
	 */
	public static UserData getUserById(final int id) {
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return DataLoader.loadRecordFromResultSet(LOAD_BY_ID, params, rsReader);		
	}
	
	/**
	 * Check if any user has the provided user id
	 * 
	 * @param userId of user to look for
	 * @return true if the user exists
	 */
	public static boolean doesUserExist(final String userId) {
		List<Object> params = new ArrayList<Object>();
		params.add(userId.toLowerCase());
		return DatabaseHelper.getIntResultByQuery(IS_USER_UNIQUE, params) == 1;		
	}
	
	/**
	 * Check if any user has the provided user id other than the one with primary key id
	 * 
	 * @param userId to search for
	 * @param id database primary key 
	 * @return true if a user exists with a different id
	 */
	public static boolean doesUserExist(final String userId, final int id) {
		List<Object> params = new ArrayList<Object>();
		params.add(userId.toLowerCase());
		params.add(id);
		return DatabaseHelper.getIntResultByQuery(IS_USER_UNIQUE2, params) == 1;		
	}
	
	/**
	 * Retrieve user by their login id
	 * 
	 * @param userId of user to retrieve
	 * @return associated user record
	 */
	public static UserData getUserByUserId(final String userId) {
		List<Object> params = new ArrayList<Object>();
		params.add(userId.toLowerCase());
		return DataLoader.loadRecordFromResultSet(LOAD_BY_USER_ID, params, rsReader);		
	}
	
	/**
	 * Retrieve a user by their (unique) reset token
	 * 
	 * @param token of user to search for
	 * @return user with the associated token
	 */
	public static UserData getUserByResetToken(final String token) {
		List<Object> params = new ArrayList<Object>();
		params.add(token);
		return DataLoader.loadRecordFromResultSet(LOAD_BY_TOKEN, params, rsReader);		
	}
	
	/**
	 * Load the record with the provided login credentials
	 * 
	 * @param userId of the authenticating user
	 * @param password of the authenticating user
	 * @return user record or null if not authenticated
	 */
	public static UserData authenticateUser(final String userId, final String password) {
		List<Object> params = new ArrayList<Object>();
		params.add(userId.toLowerCase());
		params.add(PASSWORD_SALT + password);
		return DataLoader.loadRecordFromResultSet(AUTHENTICATE_USER, params, rsReader);		
	}
	
	/**
	 * Update the user's password
	 * 
	 * @param userId of user to update
	 * @param password new password
	 */
	public static void updatePassword(final int userId, final String password) {
		DatabaseHelper.liveUpdatePassword("users", userId, "password", PASSWORD_SALT + password);
	}
	
	/**
	 * Return true of the user has a password
	 * 
	 * @param userId of password to look for
	 * @return true if the password of the user is set
	 */
	public static boolean isPasswordSet(final int userId) {
		List<Object> params = new ArrayList<Object>();
		params.add(userId);
		return DatabaseHelper.getBoolResultByQuery(IS_PASSWORD_SET, params);
	}
	
	/**
	 * Add a new user to the database 
	 * 
	 * @param user record
	 * @return id of the new record (primary key)
	 */
	public static int addUser(final UserData user) {
		return DataInserter.insertRecord(INSERT_USER, doReader, user);
	}
	
	/**
	 * Update the user record with values from the provided user object
	 * 
	 * @param user updated user data object
	 */
	public static void updateUser(final UserData user) {	
		DataInserter.updateRecord(UPDATE_USER, doReader, user);		  
	}

	@Override
	public List<Object> getParamListFromObject(final UserData user, final boolean isNew) {
		List<Object> values = new ArrayList<Object>();
		values.add(user.getUserId().toLowerCase());
		values.add(user.getLastName());
		values.add(user.getFirstName());
		values.add(user.isEnabled());
		values.add(user.isAdmin());
		
		if (!isNew) {
       	 	values.add(user.getId());
        } else {
        	values.add(user.getPassword());
        }
        
		return values;
	}

	@Override
	public UserData loadRecordFromResultSet(final ResultSet rs)throws SQLException {
		UserData user = new UserData();
		user.setId(rs.getInt("id"));
		user.setFirstName(rs.getString("firstname"));
		user.setLastName(rs.getString("lastname"));
		user.setUserId(rs.getString("userid").toLowerCase());
		user.setEnabled(rs.getBoolean("enabled"));
		user.setAdmin(rs.getBoolean("admin"));
		return user;
	}
}
