package edu.wgu.tgerlac.library.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.wgu.tgerlac.library.dao.database.DataInserter;
import edu.wgu.tgerlac.library.dao.database.DataLoader;
import edu.wgu.tgerlac.library.dao.database.interfaces.DataObjectReader;
import edu.wgu.tgerlac.library.dao.database.interfaces.ResultSetReader;
import edu.wgu.tgerlac.library.model.PatronData;

public class PatronDAO implements ResultSetReader<PatronData>, DataObjectReader<PatronData> {
	
	public static final String TABLE_NAME = "PATRONS";
	
	// define clauses
	private static final String SELECT_CLAUSE = "select * ";
	private static final String FROM_CLAUSE = "from patrons ";
	private static final String ORDER_BY = "order by lastname, firstname ";

	// define queries
	private static final String LOAD_ALL_PATRONS = SELECT_CLAUSE + FROM_CLAUSE + ORDER_BY;
	private static final String LOAD_BY_ID = SELECT_CLAUSE + FROM_CLAUSE  + "where id = ?";
	private static final String LOAD_BY_BARCODE = SELECT_CLAUSE + FROM_CLAUSE  + "where barcode = ? and active = true";
	
	private static final String INSERT_PATRON = "INSERT INTO patrons (lastname, firstname, address1, address2, city, state, zip, phone, active, barcode) VALUES(?,?,?,?,?,?,?,?,?,?)";
	private static final String UPDATE_PATRON = "UPDATE patrons SET lastname=?, firstname=?, address1=?, address2=?, city=?, state=?, zip=?, phone=?, active=?, barcode=? WHERE id=?";

	// create interface objects
	private static ResultSetReader<PatronData> rsReader;
	private static DataObjectReader<PatronData> doReader;
	
	static {
		PatronDAO dao = new PatronDAO();
		rsReader = dao;
		doReader = dao;
	}
	
	public static List<PatronData> getAllPatrons() {
		List<Object> params = new ArrayList<>();
		return DataLoader.loadListFromResultSet(LOAD_ALL_PATRONS, params, rsReader); 
	}
	
	public static PatronData getPatronById(final int id) {
		List<Object> params = new ArrayList<Object>();
        params.add(id);
        return DataLoader.loadRecordFromResultSet(LOAD_BY_ID, params, rsReader); 
	}
	
	public static PatronData getPatronByBarcode(final String barcode) {
		List<Object> params = new ArrayList<Object>();
        params.add(barcode);
        return DataLoader.loadRecordFromResultSet(LOAD_BY_BARCODE, params, rsReader); 
	}
	
	public static int insertPatron(final PatronData patron) {
		return DataInserter.insertRecord(INSERT_PATRON, doReader, patron);
	}
	
	public static void updatePatron(final PatronData patron) {
		DataInserter.updateRecord(UPDATE_PATRON, doReader, patron);
	}
	
	public List<Object> getParamListFromObject(final PatronData patron, final boolean isNew) {
		List<Object> values = new ArrayList<Object>();
				
		values.add(patron.getLastName());
		values.add(patron.getFirstName());
		values.add(patron.getAddress1());
		values.add(patron.getAddress2());
		values.add(patron.getCity());
		values.add(patron.getState());
		values.add(patron.getZip());
		values.add(patron.getPhone());
		values.add(patron.isActive());
		values.add(patron.getBarcode());
		
        if (!isNew) {
        	values.add(patron.getId());
        }

        return values;
	}

	public PatronData loadRecordFromResultSet(final ResultSet rs)throws SQLException {
		PatronData patron = new PatronData();
		patron.setId(rs.getInt("id"));
		patron.setLastName(rs.getString("lastname"));
		patron.setFirstName(rs.getString("firstname"));
		patron.setActive(rs.getBoolean("active"));
		patron.setAddress1(rs.getString("address1"));
		patron.setAddress2(rs.getString("address2"));
		patron.setCity(rs.getString("city"));
		patron.setState(rs.getString("state"));
		patron.setZip(rs.getString("zip"));
		patron.setPhone(rs.getString("phone"));
		patron.setBarcode(rs.getString("barcode"));
		return patron;
	}
}
