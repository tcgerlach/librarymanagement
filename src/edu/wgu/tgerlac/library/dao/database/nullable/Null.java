package edu.wgu.tgerlac.library.dao.database.nullable;

import java.sql.Types;

public class Null {
	
	public static final Null Char = new Null(Types.CHAR);
	public static final Null Date = new Null(Types.DATE);
	public static final Null Double = new Null(Types.DOUBLE);
	public static final Null Integer = new Null(Types.INTEGER);
	public static final Null Time = new Null(Types.TIME);
	public static final Null Timestamp = new Null(Types.TIMESTAMP);
	public static final Null String = new Null(Types.VARCHAR);
	
	private int type;
	
	private Null(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}
}
