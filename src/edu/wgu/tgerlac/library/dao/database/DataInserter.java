package edu.wgu.tgerlac.library.dao.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import edu.wgu.tgerlac.library.dao.database.interfaces.DataObjectReader;

/**
 * Generic functions to insert objects into the database
 * 
 * @author tcgerlach
 */
public class DataInserter {
	
	/**
	 * Add a new record to the database.
	 * NOTE This needs to write data AND retrieve ID. As such, it is synchronized
	 * 
	 * @param sql query to insert record
	 * @param reader used to retrieve values from object
	 * @param object to insert into database 
	 * @return primary key (id) for the new record
	 */
	public synchronized static <T> int insertRecord(String sql, DataObjectReader<T> reader, T object) {
		int lastId;
		
		Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = DBConnector.getConnection();                
            List<Object> values = reader.getParamListFromObject(object, true);
            DatabaseHelper.writeData(sql, values, "Error adding record");
            lastId = DatabaseHelper.getIntResultByQuery(DatabaseHelper.MYSQL_GET_LAST_ID);
        } finally {
            DBConnector.closeConnection(null, ps, conn);
        }
        return lastId;
	}
	
	/**
	 * Update a database record.
	 * 
	 * @param sql query for database update
	 * @param reader to read the fields from the java object
	 * @param object to be updated in the database
	 */
	public static <T> void updateRecord(String sql, DataObjectReader<T> reader, T object) {
		 Connection conn = null;
         PreparedStatement ps = null;
         try {
             conn = DBConnector.getConnection();                
             List<Object> values = reader.getParamListFromObject(object, false);
             DatabaseHelper.writeData(sql, values, "Error updating record");
         } finally {
             DBConnector.closeConnection(null, ps, conn);
         }
	}
}
