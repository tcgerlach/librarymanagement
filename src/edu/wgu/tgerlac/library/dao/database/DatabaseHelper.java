package edu.wgu.tgerlac.library.dao.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.wgu.tgerlac.library.dao.database.nullable.Null;
import edu.wgu.tgerlac.library.shared.LibraryConstants;

/**
 * Shared database functions
 * 
 * @author tcgerlach
 */
public class DatabaseHelper {

	private static Logger logger = LogManager.getLogger(LibraryConstants.LOGGER);
	
	/**
	 * SQL query to get the last record insertion id
	 */
	public static final String MYSQL_GET_LAST_ID = "SELECT LAST_INSERT_ID()";	

	/**
	 * Run a SQL query and return the integer result. 
	 * This can be used to calculate sums or other data
	 * that is not cleanly represented by a java object.
	 * 
	 * @param query to run
	 * @return integer value of query
	 */
	public static int getIntResultByQuery(String query) {
		List<Object> params = new ArrayList<Object>();
		return getIntResultByQuery(query, params);
	}

	/**
	 * Run a SQL query and return the integer result. 
	 * This can be used to calculate sums or other data
	 * that is not cleanly represented by a java object.
	 * 
	 * @param query to run
	 * @param dataFields query parameters for provided query
	 * @return integer value of query
	 */
	public static int getIntResultByQuery(String query, List<Object> dataFields) {
		int x = 0;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = DBConnector.getConnection();
			conn.setAutoCommit(true);

			ps = conn.prepareStatement(query);
			addStatementParams(ps, dataFields);
			
			if (ps.execute()) {
				rs = ps.getResultSet();
				while (rs.next()) {
					x = rs.getInt(1);
				}
			}
		} catch (SQLException e) {
			logger.warn("Error getting int result", e);
		} finally {
			DBConnector.closeConnection(rs, ps, conn);
		}

		return x;
	}
	
	/**
	 * Run a SQL query and return the boolean result. 
	 * 
	 * @param query to run
	 * @param dataFields query parameters for provided query
	 * @return boolean value of query
	 */
	public static boolean getBoolResultByQuery(String query, List<Object> dataFields) {
		boolean x = false;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = DBConnector.getConnection();
			conn.setAutoCommit(true);

			ps = conn.prepareStatement(query);
			addStatementParams(ps, dataFields);
			
			if (ps.execute()) {
				rs = ps.getResultSet();
				while (rs.next()) {
					x = rs.getBoolean(1);
				}
			}
		} catch (SQLException e) {
			logger.warn("Error getting int result", e);
		} finally {
			DBConnector.closeConnection(rs, ps, conn);
		}

		return x;
	}
	
	private static void addStatementParams(PreparedStatement ps, List<Object> dataFields) throws SQLException {
		for (int i = 0; i < dataFields.size(); ++i) {
			Object fieldData = dataFields.get(i);
			if (fieldData instanceof Null) {
				int nullType = ((Null)fieldData).getType();
				ps.setNull(i + 1, nullType);
			} else if (fieldData instanceof String) {
				ps.setString(i + 1, (String) fieldData);
			} else if (fieldData instanceof Integer) {
				ps.setInt(i + 1, (Integer) fieldData);
			} else if (fieldData instanceof Boolean) {
				ps.setBoolean(i + 1, (Boolean) fieldData);
			} else if (fieldData instanceof Timestamp) {
				ps.setTimestamp(i + 1, (Timestamp) fieldData);
			} else if (fieldData instanceof Float) {
				ps.setFloat(i + 1, (Float) fieldData);
			} else {
				logger.error("DatabaseHelper Unknown database field type");
			}
		}
	}

	/**
	 * Write data to the database
	 * 
	 * @param SQL query to execute
	 * @param dataFields data to populate query fields
	 * @param errorMessage message to display if write fails
	 */
	public static void writeData(String SQL, List<Object> dataFields, String errorMessage) {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DBConnector.getConnection();
			conn.setAutoCommit(true);

			ps = conn.prepareStatement(SQL);
			addStatementParams(ps, dataFields);
			ps.execute();
		} catch (SQLException e) {
			logger.warn(errorMessage + " - Problem with SQL: " + SQL, e);
		} finally {
			DBConnector.closeConnection(null, ps, conn);
		}
	}

	/**
	 * Retrieve a value from a table based on ID
	 * 
	 * @param table database table to query
	 * @param field database field to retrieve
	 * @param id primary key of table
	 * @return
	 */
	public static Object getFieldValue(String table, String field, int id) {
		Object value = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			conn = DBConnector.getConnection();
			conn.setAutoCommit(true);

			ps = conn.prepareStatement("select " + field + " from " + table + " where id = ?");
			ps.setInt(1, id);

			if (ps.execute()) {
				rs = ps.getResultSet();
				while (rs.next()) {
					value = rs.getObject(field);
				}
			}
		} catch (SQLException e) {
			logger.warn("Error loading field from table", e);
		} finally {
			DBConnector.closeConnection(rs, ps, conn);
		}

		return value;
	}

	/**
	 * Update a field in a table
	 * 
	 * @param table database table to update
	 * @param id primary key of record to update
	 * @param field name of field to update
	 * @param value new value for field
	 */
	public static void liveUpdate(String table, int id, String field, Object value) {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DBConnector.getConnection();
			ps = conn.prepareStatement("UPDATE " + table + " SET " + field + " = ? where id = ?");

			ps.setObject(1, value);
			ps.setInt(2, id);

			ps.execute();
		} catch (SQLException e) {
			logger.warn("Error updating live data", e);
		} finally {
			DBConnector.closeConnection(null, ps, conn);
		}
	}
	
	/**
	 * Update a password field. The value will be inserted as a hash.
	 * 
	 * @param table database table with password field
	 * @param id primary key of record
	 * @param field name of field
	 * @param value new password value
	 */
	public static void liveUpdatePassword(String table, int id, String field, Object value) {
		Connection conn = null;
		PreparedStatement ps = null;
	
		try {
			conn = DBConnector.getConnection();
			ps = conn.prepareStatement("UPDATE " + table + " SET " + field + " = md5(?) where id = ?");
	
			ps.setObject(1, value);
			ps.setInt(2, id);
	
			ps.execute();
		} catch (SQLException e) {
			logger.warn("Error updating live data", e);
		} finally {
			DBConnector.closeConnection(null, ps, conn);
		}
	}
}
