package edu.wgu.tgerlac.library.dao.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.wgu.tgerlac.library.dao.database.interfaces.ResultSetReader;
import edu.wgu.tgerlac.library.shared.LibraryConstants;

public class DataLoader {
	
	private static Logger logger = LogManager.getLogger(LibraryConstants.LOGGER);
	
	public static <T> List<T> loadListFromResultSet(String sql, List<Object> dataFields, ResultSetReader<T> rsReader) {
		List<T> records = new ArrayList<T>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnector.getConnection();
			ps = conn.prepareStatement(sql);
			
			if (dataFields != null) {
				for(int i = 0; i < dataFields.size(); ++i) {
					Object fieldData = dataFields.get(i);
					if(fieldData instanceof String) {
						ps.setString(i+1, (String)fieldData);
					} else if (fieldData instanceof Integer) {
						ps.setInt(i+1, (Integer)fieldData);
					} else if (fieldData instanceof Boolean) {
						ps.setString(i+1, (Boolean)fieldData ? "1" : "0");			
					} else if (fieldData instanceof Timestamp) {
						ps.setTimestamp(i+1, (Timestamp)fieldData);
					} else if (fieldData instanceof Long) {
						ps.setLong(i+1, (Long)fieldData);
					} else if (fieldData instanceof java.util.Date) {
						java.util.Date incomingDate = (java.util.Date)fieldData;
						java.sql.Date sqlDate = new java.sql.Date(incomingDate.getTime());
						ps.setDate(i+1, sqlDate);					
					} else {
						throw new RuntimeException("Unidentified data type");
					}				
				}	
			}
			
			if (ps.execute()) {
				rs = ps.getResultSet();
				while (rs.next()) {
					records.add(rsReader.loadRecordFromResultSet(rs));
				}
			}
		} catch (SQLException e) {
			logger.error("Error loading data", e);
		} finally {
			DBConnector.closeConnection(rs, ps, conn);
		}
				
		return records;
	}
	
	public static <T> T loadRecordFromResultSet(String sql, List<Object> dataFields, ResultSetReader<T> rsReader) {
		T data = null;
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnector.getConnection();
			ps = conn.prepareStatement(sql);
			
			for(int i = 0; i < dataFields.size(); ++i) {
				Object fieldData = dataFields.get(i);
				if(fieldData instanceof String) {
					ps.setString(i+1, (String)fieldData);
				} else if (fieldData instanceof Integer) {
					ps.setInt(i+1, (Integer)fieldData);
				} else if (fieldData instanceof Boolean) {
					ps.setString(i+1, (Boolean)fieldData ? "1" : "0");			
				} else if (fieldData instanceof Timestamp) {
					ps.setTimestamp(i+1, (Timestamp)fieldData);
				} else if (fieldData instanceof Long) {
					ps.setLong(i+1, (Long)fieldData);
				} else {
					throw new RuntimeException("Unidentified data type");
				}				
			}	
			
			if (ps.execute()) {
				rs = ps.getResultSet();
				while (rs.next()) {
					data = rsReader.loadRecordFromResultSet(rs);
				}
			}
		} catch (SQLException e) {
			logger.error("Error loading data", e);
		} finally {
			DBConnector.closeConnection(rs, ps, conn);
		}
				
		return data;
	}
}
