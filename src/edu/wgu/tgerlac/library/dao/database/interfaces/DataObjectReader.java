package edu.wgu.tgerlac.library.dao.database.interfaces;

import java.util.List;

/**
 * Helper interface for the DataInserter class
 * 
 * @author tcgerlach
 *
 * @param <T> type of object to read
 */
public interface DataObjectReader<T> {

	/**
	 * Retrieve a parameter list from the provided object.
	 * This is used by the data inserter to create the database query.
	 * 
	 * @param object data object to retrieve values from
	 * @param isNew true if this is a new record
	 * @return list of name field values
	 */
	public List<Object> getParamListFromObject(T object, boolean isNew);
}
