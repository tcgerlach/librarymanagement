package edu.wgu.tgerlac.library.dao.database.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Helper interface for the DataLoader class
 * 
 * @author tcgerlach
 *
 * @param <T> type of object to load
 */
public interface ResultSetReader<T> {
	
	/**
	 * Read and return a record from the result set
	 * 
	 * @param rs result set to read record from
	 * @return java record from the database result set
	 * @throws SQLException if a SQL error has occurred
	 */
	public T loadRecordFromResultSet(ResultSet rs) throws SQLException;
}
