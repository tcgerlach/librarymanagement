package edu.wgu.tgerlac.library.dao.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import edu.wgu.tgerlac.library.shared.LibraryConstants;

public class DBConnector {

	private static Logger logger = LogManager.getLogger(LibraryConstants.LOGGER);
	private static final String DATASOURCE_TOMCAT = "jdbc/library";

	private static int connectionCount = 0;
	private static int maxConnectionCount = 0;

	private static boolean useJndiGetConnection = true;

    public static Connection getConnection() {
        if (useJndiGetConnection) {
        	return getJNDIBasedConnection();
        } else {
            return getStandAloneConnection();
        }
    }

    private static Connection getStandAloneConnection() {
        Connection conn = null;

        try {
          Class.forName("com.mysql.jdbc.Driver");

          Properties properties = new Properties();
          properties.put("user", "root");
          properties.put("password", "");
          String url = "jdbc:mysql://localhost/library?user=root";

          conn = DriverManager.getConnection(url, properties);
        } catch (ClassNotFoundException e) {
             e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
    	}

        return conn;
    }
    
    private static Connection getJNDIBasedConnection() {
    	 Connection conn = null;
         try {
        	 Context ctx = new InitialContext();
        	 Context envContext  = (Context)ctx.lookup("java:/comp/env");
        	 DataSource ds = (DataSource)envContext.lookup(DATASOURCE_TOMCAT);
        	 conn = ds.getConnection();
        	 
             ++connectionCount;
             while (connectionCount > maxConnectionCount) {
                 ++maxConnectionCount;
                 logger.info("maxConnectionCount = " + maxConnectionCount);
             }
         } catch (NamingException e) {
             logger.error("Naming exception loading db", e);
         } catch (SQLException e) {
             logger.error("SQL exception connecting to db", e);
         }
         return conn;
    }

    public static void closeConnection(ResultSet rs, PreparedStatement ps, Connection conn) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                logger.warn("Error closing result statement", e);
            }
        }

        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                logger.warn("Error closing prepared statement", e);
            }
        }

        if (conn != null) {
            try {
                conn.close();
                --connectionCount;
            } catch (SQLException e) {
                logger.warn("Error closing connection", e);
            }
        }
   }

    public static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
                --connectionCount;
            } catch (SQLException e) {
                logger.warn("Error closing connection", e);
            }
        }
    }

    public static void closePreparedStatementResultSet(PreparedStatement ps, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                logger.warn("Error closing ResultSet", e);
            }
        }

        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                logger.warn("Error closing prepared statement", e);
            }
        }
    }

    public static boolean isUseJndiGetConnection() {
        return useJndiGetConnection;
    }

    public static void setUseJndiGetConnection(boolean useJndiGetConnection) {
        DBConnector.useJndiGetConnection = useJndiGetConnection;
    }
}
