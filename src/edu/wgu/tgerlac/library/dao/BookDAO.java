package edu.wgu.tgerlac.library.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.wgu.tgerlac.library.dao.database.DataInserter;
import edu.wgu.tgerlac.library.dao.database.DataLoader;
import edu.wgu.tgerlac.library.dao.database.interfaces.DataObjectReader;
import edu.wgu.tgerlac.library.dao.database.interfaces.ResultSetReader;
import edu.wgu.tgerlac.library.model.BookData;

public class BookDAO implements ResultSetReader<BookData>, DataObjectReader<BookData> {
	
	/*
	 * NOTE: The book table is not normalized. In a larger, enterprise system, the author
	 * data would be placed in a separate table. However, for small libraries, the overhead
	 * of managing the author data separately from the books would generate unnecessary
	 * work. Thus, the author names are stored directly in the book table. Likewise,
	 * the subject is contained in the book table. For small library systems, this should
	 * perform well and be substantially easier to manage.
	 */
	
	public static final String TABLE_NAME = "BOOKS";
	
	// define clauses
	private static final String SELECT_CLAUSE = "select * ";
	private static final String FROM_CLAUSE = "from BOOK_VIEW b ";
	private static final String ORDER_BY = "order by title ";

	// define queries
	private static final String LOAD_ALL_BOOKS = SELECT_CLAUSE + FROM_CLAUSE  + ORDER_BY;
	private static final String LOAD_BY_ID = SELECT_CLAUSE + FROM_CLAUSE  + "where b.id = ? ";
	private static final String LOAD_BY_ISBN = SELECT_CLAUSE + FROM_CLAUSE + " where isbn = ? ";
	private static final String SEARCH_BASE = SELECT_CLAUSE + FROM_CLAUSE + "where ";
	
	private static final String INSERT_BOOK = "INSERT INTO books (title, author1, author2, subject1, subject2, subject3, isbn, synopsis, location) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String UPDATE_BOOK = "UPDATE books SET title=?, author1=?, author2=?, subject1=?, subject2=?, subject3=?, isbn=?, synopsis=?, location=? WHERE id=?";

	private static final String LOAD_BOOKS_FOR_PATRON =  
			"select b.* " + 
			"from checkouts co " + 
			"left outer join copies c on c.id = co.copy_id " + 
			"left outer join book_view b on b.id = c.book_id " +
			"where patron_id = ? and indate is null";
	
	// create interface objects
	private static ResultSetReader<BookData> rsReader;
	private static DataObjectReader<BookData> doReader;
	
	static {
		BookDAO dao = new BookDAO();
		rsReader = dao;
		doReader = dao;
	}
	
	public static BookData getBookByISBN(String ISBN) {
		List<Object> params = new ArrayList<>();
		params.add(ISBN);
		return DataLoader.loadRecordFromResultSet(LOAD_BY_ISBN, params, rsReader);
	}
	
	public static List<BookData> getAllBooks() {
		List<Object> params = new ArrayList<>();
		return DataLoader.loadListFromResultSet(LOAD_ALL_BOOKS, params, rsReader); 
	}
	
	public static List<BookData> getBooksByPatron(int patronId) {
		List<Object> params = new ArrayList<>();
		params.add(patronId);
		return DataLoader.loadListFromResultSet(LOAD_BOOKS_FOR_PATRON, params, rsReader);
	}
	
	public static List<BookData> searchBooks(String title, String author, String isbn, String subject) {
		List<Object> params = new ArrayList<Object>();
		
		String whereClause = "";
		
		// check each field
		if (title != null && title.length() > 0) {
			whereClause += "lower(title) like ? ";
			params.add("%" + title.toLowerCase() + "%");
		}
		
		if (author != null && author.length() > 0) {
			if (whereClause.length() > 0) {
				whereClause += " and ";
			}
			whereClause += "(lower(author1) like ? or lower(author2) like ?) ";
			params.add("%" + author.toLowerCase() + "%");
			params.add("%" + author.toLowerCase() + "%");
		}
		
		if (isbn != null && isbn.length() > 0) {
			if (whereClause.length() > 0) {
				whereClause += " and ";
			}
			whereClause += "isbn = ? ";
			params.add(isbn.toLowerCase());
		}
		
		if (subject != null && subject.length() > 0) {
			if (whereClause.length() > 0) {
				whereClause += " and ";
			}
			whereClause += "(lower(subject1) = ? or lower(subject2) = ? or lower(subject3) = ?) ";
			params.add(subject.toLowerCase());
			params.add(subject.toLowerCase());
			params.add(subject.toLowerCase());
		}
		
		// create query
		String query;
		if (whereClause.length() > 0) {
			query = SEARCH_BASE + whereClause + ORDER_BY;
		} else {
			query = LOAD_ALL_BOOKS;
		}
		
		// return data
		return DataLoader.loadListFromResultSet(query, params, rsReader); 	
	}
	
	public static BookData getBookById(final int id) {
		List<Object> params = new ArrayList<Object>();
        params.add(id);
        return DataLoader.loadRecordFromResultSet(LOAD_BY_ID, params, rsReader); 
	}
	
	public static int insertBook(final BookData book) {
		return DataInserter.insertRecord(INSERT_BOOK, doReader, book);
	}
	
	public static void updateBook(final BookData book) {
		DataInserter.updateRecord(UPDATE_BOOK, doReader, book);
	}
	
	public List<Object> getParamListFromObject(final BookData book, final boolean isNew) {
		List<Object> values = new ArrayList<Object>();
				
		values.add(book.getTitle());
		values.add(book.getAuthor1());
		values.add(book.getAuthor2());
		values.add(book.getSubject1());
		values.add(book.getSubject2());
		values.add(book.getSubject3());
		values.add(book.getIsbn());
		values.add(book.getSynopsis());
		values.add(book.getLocation());
		
        if (!isNew) {
        	values.add(book.getId());
        }

        return values;
	}

	public BookData loadRecordFromResultSet(final ResultSet rs)throws SQLException {
		BookData book = new BookData();
		book.setAuthor1(rs.getString("author1"));
		book.setAuthor2(rs.getString("author2"));
		book.setSubject1(rs.getString("subject1"));
		book.setSubject2(rs.getString("subject2"));
		book.setSubject3(rs.getString("subject3"));
		book.setId(rs.getInt("id"));
		book.setIsbn(rs.getString("isbn"));
		book.setSynopsis(rs.getString("synopsis"));
		book.setTitle(rs.getString("title"));
		book.setCopies(rs.getInt("copies"));
		book.setCheckedOut(rs.getInt("checkedout"));
		book.setLocation(rs.getString("location"));
		
		return book;
	}
}
