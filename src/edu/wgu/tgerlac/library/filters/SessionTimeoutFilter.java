package edu.wgu.tgerlac.library.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.wgu.tgerlac.library.shared.WebLogger;

public class SessionTimeoutFilter implements Filter {
	private String timeoutPage = null;
    private FilterConfig filterConfig = null;

    public SessionTimeoutFilter() {

    }

    public void init(FilterConfig filterConfig) throws ServletException {
        WebLogger.info("Initializing SessionTimeoutFilter!");
        this.filterConfig = filterConfig;
        this.timeoutPage = filterConfig.getInitParameter("timeoutPage");
        WebLogger.info("timeoutPage -> " + this.timeoutPage);
    }

    public void destroy() {
        this.filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;

            // is session invalid?
            if (this.timeoutPage != null && !this.timeoutPage.equals("") && isSessionInvalid(httpRequest)) {
                String timeoutUrl = filterConfig.getServletContext().getContextPath() + this.timeoutPage;
                WebLogger.info("Session timed out.");
                WebLogger.info("Redirecting to " + timeoutUrl);
                httpResponse.sendRedirect(timeoutUrl);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    private boolean isSessionInvalid(HttpServletRequest request) {
        return (request.getRequestedSessionId() != null) && !request.isRequestedSessionIdValid();
    }
}
