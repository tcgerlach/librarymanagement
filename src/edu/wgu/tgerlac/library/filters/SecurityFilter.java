package edu.wgu.tgerlac.library.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.wgu.tgerlac.library.shared.WebLogger;

/**
 * Filter to handle redirect to HTTPS
 * 
 * @author tcgerlach
 */
public class SecurityFilter implements Filter {

    private String secureProtocol = null;
    private String nonSecureProtocol = null;
    private boolean doSecure = true;

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //need to get the requested url
        HttpServletRequest httpReq = (HttpServletRequest)request;
        HttpServletResponse httpResp = (HttpServletResponse)response;

        String requestedUrl = httpReq.getRequestURL().toString();
        String requestedDocOrg = httpReq.getRequestURI().substring(httpReq.getContextPath().length() + 1);

        if(requestedDocOrg.length() > 0) {
            if(doSecure) {
            	//it is so we need to check protocol
                if(!requestedUrl.startsWith(this.secureProtocol)) {
                    //we aren't secure and we need to be
                    String newURL = requestedUrl.replaceFirst(this.nonSecureProtocol, this.secureProtocol);
                    WebLogger.info("Redirecting to " + newURL);
                    httpResp.sendRedirect(newURL);
                    return;
                }
            }
        }

        chain.doFilter(request, response);
    }

    public void destroy() {
            // DO NOTHING
    }

    public void init(FilterConfig config) throws ServletException {
        secureProtocol = config.getInitParameter("secureProtocol");
        if(secureProtocol == null) {
        	throw new ServletException("secureProtocol init param missing");
        }

        nonSecureProtocol = config.getInitParameter("nonSecureProtocol");
        if(nonSecureProtocol == null) {
        	throw new ServletException("nonSecureProtocol init param missing");
        }

        String strDoSecure = config.getInitParameter("doSecure");

        if(strDoSecure == null) {
        	throw new ServletException("doSecure init param missing");
        } else {
        	this.doSecure = Boolean.valueOf(strDoSecure);
        }
    }
}
