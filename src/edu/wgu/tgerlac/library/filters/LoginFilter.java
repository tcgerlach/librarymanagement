package edu.wgu.tgerlac.library.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.wgu.tgerlac.library.handler.AppHandler;
import edu.wgu.tgerlac.library.shared.WebLogger;

public class LoginFilter implements Filter {
    private String[] protectedPaths = null;
    private String loginPage = null;
    private String errorPage = null;
    private FilterConfig filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        WebLogger.info("Intializing LoginFilter");
        loginPage = filterConfig.getInitParameter("loginPage");
        String paths = filterConfig.getInitParameter("protectedPaths");
        WebLogger.info("Protected paths: " + paths);
        if (paths != null) {
                protectedPaths = paths.split(",");
        }
        errorPage = filterConfig.getInitParameter("errorPage");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        WebLogger.info("In LoginFilter.doFilter");
        boolean redirectSent = false;
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpResponse = (HttpServletResponse)response;
        String reqPath = httpRequest.getServletPath();
        HttpSession session = httpRequest.getSession();
        String originalURL = (String)session.getAttribute("originalURL");
        if (originalURL == null) {
                originalURL = httpRequest.getRequestURI();
        }
        WebLogger.info("OriginalURL: " + originalURL);

        boolean allowed = true;
        for (int i = 0; i < protectedPaths.length; i++) {
            if (reqPath.startsWith(protectedPaths[i].trim())) {
                allowed = false;
                break;
            }
        }
        
        if (!allowed) {
            Object appObj = session.getAttribute("appHandler");
            if (appObj == null || ((AppHandler)appObj).getUser()== null) {
                ServletContext context = this.filterConfig.getServletContext();
                RequestDispatcher rd = context.getRequestDispatcher(loginPage);
                session.setAttribute("originalURL", originalURL);
                if (rd == null) {
                    httpResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Login page doesn't exist!");
                } else {
                    rd.forward(httpRequest, httpResponse);
                }
                redirectSent = true;
            }
        }
        
        if (!redirectSent) {
            try {
                chain.doFilter(request, response);
            } catch(Exception e) {
                WebLogger.error("LoginFilter caused exception: " + e.getMessage());

                if (errorPage != null) {
                    // Redirect to the error page
                    ((HttpServletResponse)response).sendRedirect(errorPage);
                } else {
                    throw new ServletException("Error in Security Filter");
                }
            }
        }
    }

    public void destroy() {
    	this.filterConfig = null;
    }
}
