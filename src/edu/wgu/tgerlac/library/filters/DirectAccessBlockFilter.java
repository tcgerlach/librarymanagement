package edu.wgu.tgerlac.library.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.wgu.tgerlac.library.shared.WebLogger;

/**
 * Prevent direct access to pages this filter is applied to.
 * Redirect the user to the login page.
 * 
 * @author tcgerlach
 */
public class DirectAccessBlockFilter implements Filter {

    private String loginPage = "/login.faces";

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);

        if(session == null || (session.getAttribute("user") == null && !httpRequest.getServletPath().contains(loginPage))) {           
            httpResponse.sendRedirect(httpRequest.getContextPath() + loginPage);            
            WebLogger.info("Redirecting to login page");
        } else {
        	chain.doFilter(request, response);
        }
    }

    public void destroy() {
            // DO NOTHING
    }

    public void init(FilterConfig arg0) throws ServletException {
            // DO NOTHING
    }
}

