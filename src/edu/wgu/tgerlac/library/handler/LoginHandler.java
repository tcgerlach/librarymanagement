package edu.wgu.tgerlac.library.handler;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.wgu.tgerlac.library.dao.UserDAO;
import edu.wgu.tgerlac.library.model.UserData;
import edu.wgu.tgerlac.library.shared.WebLogger;

@SuppressWarnings("serial")
@ManagedBean
@RequestScoped
public class LoginHandler implements Serializable {

	private String userName;
	private String password;
	
	private static final String LOGIN_PAGE = "/login";
	private static final String SEARCH_PAGE = "/search";
	
	@ManagedProperty(value="#{appHandler}")
	private AppHandler appHandler;
	
	public String processLogin() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();		
		HttpSession session = request.getSession();
		
		UserData user = UserDAO.authenticateUser(userName, password);

		if (user != null) {
			if (user.isEnabled()) {
				String msg = "User successfully logged in";				
				WebLogger.debug(user, msg);
				
				appHandler.setLoggedInUser(user);
				session.setAttribute("user", user);
				session.setAttribute("appHandler", appHandler);
				
				return appHandler.goHome();
			} else {								
				String msg = "User " + userName + " failed login attempt - account disabled";
				WebLogger.debug(msg);
				
				appHandler.displayError("Login Disabled", "Login disabled for " + userName);
				return LOGIN_PAGE;
			}
		} else {
			String msg = "User " + userName + " failed login attempt - not authenticated";
			WebLogger.debug(msg);
			
			appHandler.displayError("Login Failed", "Login failed for " + userName);  			
			return LOGIN_PAGE;
		}
	}
	
	public String processLogout() {	
		WebLogger.info(appHandler.getUser(), "Logging out");
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return SEARCH_PAGE;
	}
	
	public AppHandler getAppHandler() {
		return appHandler;
	}

	public void setAppHandler(AppHandler appHandler) {
		this.appHandler = appHandler;
	}

	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
