package edu.wgu.tgerlac.library.handler;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import edu.wgu.tgerlac.library.dao.BookDAO;
import edu.wgu.tgerlac.library.dao.PatronDAO;
import edu.wgu.tgerlac.library.model.BookData;
import edu.wgu.tgerlac.library.model.PatronData;
import edu.wgu.tgerlac.library.shared.WebLogger;

@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class PatronHandler implements Serializable {
	
	private List<PatronData> patrons;
	private List<BookData> patronBooks;
	private PatronData selectedPatron = new PatronData();

	private static final String VIEW_PAGE = "/modules/patrons/patronview";
	
	@ManagedProperty(value="#{appHandler}")
	private AppHandler appHandler;	
	
	public AppHandler getAppHandler() {
		return appHandler;
	}

	public void setAppHandler(AppHandler appHandler) {
		this.appHandler = appHandler;
	}
	
	public List<PatronData> getPatrons() {
		return patrons;
	}

	public void setPatrons(List<PatronData> patrons) {
		this.patrons = patrons;
	}

	public PatronData getSelectedPatron() {
		return selectedPatron;
	}
	
	public void setSelectedPatron(PatronData patron) {
		this.selectedPatron = patron;
		
		// update patron books
		this.patronBooks = BookDAO.getBooksByPatron(this.selectedPatron.getId());
	}

	public List<BookData> getPatronBooks() {
		return patronBooks;
	}

	public void setPatronBooks(List<BookData> patronBooks) {
		this.patronBooks = patronBooks;
	}

	public String processViewPatrons() {
		WebLogger.info(appHandler.getUser(), "Viewing all patrons");
		patrons = PatronDAO.getAllPatrons();
		return VIEW_PAGE;
	}
	
	public void ajaxAddPatron() {
		WebLogger.info(appHandler.getUser(), "Adding a new patron");
		selectedPatron = new PatronData();
	}
	
	public void ajaxSavePatron() {
		WebLogger.info(appHandler.getUser(), "Saving patron " + selectedPatron.getFirstLast());
		if (selectedPatron.getId() > 0) {
			PatronDAO.updatePatron(selectedPatron);
		} else {
			int id = PatronDAO.insertPatron(selectedPatron);
			selectedPatron.setId(id);
			patrons.add(selectedPatron);
			Collections.sort(patrons);
		}
	}
}
