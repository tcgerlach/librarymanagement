package edu.wgu.tgerlac.library.handler;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import edu.wgu.tgerlac.library.dao.CheckoutDAO;
import edu.wgu.tgerlac.library.dao.CopyDAO;
import edu.wgu.tgerlac.library.dao.PatronDAO;
import edu.wgu.tgerlac.library.model.CheckoutData;
import edu.wgu.tgerlac.library.model.CopyData;
import edu.wgu.tgerlac.library.model.PatronData;
import edu.wgu.tgerlac.library.shared.WebLogger;

@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class CheckoutHandler implements Serializable {
	
	private List<CheckoutData> checkouts;
	
	// search fields
	private String checkoutPatron;
	private String checkoutBook;
	
	private static final String ACTIVITY = "/modules/checkouts/activity";
	private static final String CHECKOUT = "/modules/checkouts/checkout";
	private static final String CHECKIN = "/modules/checkouts/checkin";

	
	@ManagedProperty(value="#{appHandler}")
	private AppHandler appHandler;	
	
	public AppHandler getAppHandler() {
		return appHandler;
	}

	public void setAppHandler(AppHandler appHandler) {
		this.appHandler = appHandler;
	}

	public List<CheckoutData> getCheckouts() {
		return checkouts;
	}

	public void setCheckouts(List<CheckoutData> checkouts) {
		this.checkouts = checkouts;
	}

	public String getCheckoutPatron() {
		return checkoutPatron;
	}

	public void setCheckoutPatron(String checkoutPatron) {
		this.checkoutPatron = checkoutPatron;
	}

	public String getCheckoutBook() {
		return checkoutBook;
	}

	public void setCheckoutBook(String checkoutBook) {
		this.checkoutBook = checkoutBook;
	}

	public String processViewActivity() {
		checkouts = null;
		return ACTIVITY;
	}
	
	public String processCheckoutBook() {
		this.checkoutBook = "";
		this.checkoutPatron = "";
		return CHECKOUT;
	}
	
	public String processCheckinBook() {
		this.checkoutBook = "";
		return CHECKIN;
	}
	
	public void ajaxProcessCheckout() {
		WebLogger.info(appHandler.getUser(), "Checking out " + this.checkoutBook + " to " + this.checkoutPatron);
		
		PatronData patron = PatronDAO.getPatronByBarcode(this.checkoutPatron);
		if (patron == null) {
			appHandler.displayError("Invalid Patron", "Entered barcode is invalid");
			return;
		}
		
		CopyData copy = CopyDAO.getCopyByBarcode(this.checkoutBook);
		if (copy == null) {
			appHandler.displayError("Invalid Book", "Book not found");
			return;
		}
		
		if (CheckoutDAO.isBookCheckedOut(this.checkoutBook)) {
			appHandler.displayError("Already Checked Out", "This book is already checked out");
			return;
		}
		
		int pastDue = CheckoutDAO.getPastDueCountForPatron(patron.getId());
		if (pastDue > 0) {
			appHandler.displayError("Patron Past Due", "Patron has books past due");
			return;
		}
		
		// if we got here, everything is good to check book out to patron
		CheckoutDAO.checkoutBook(copy.getId(), patron.getId());
		appHandler.displayMessage("Book Checked Out", "Book successfully checked out to patron");
	}
	
	public void ajaxProcessCheckin() {
		WebLogger.info(appHandler.getUser(), "Checking in book " + this.checkoutBook);
		CopyData copy = CopyDAO.getCopyByBarcode(this.checkoutBook);
		if (copy == null) {
			appHandler.displayError("Invalid Book", "Book not found");
			return;
		}
		
		// if we got here, everything is good to check book in
		CheckoutDAO.checkinBook(this.checkoutBook);
		appHandler.displayMessage("Book Checked In", "Book successfully checked in");
	}
}
