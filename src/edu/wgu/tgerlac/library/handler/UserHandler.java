package edu.wgu.tgerlac.library.handler;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import edu.wgu.tgerlac.library.dao.UserDAO;
import edu.wgu.tgerlac.library.model.UserData;
import edu.wgu.tgerlac.library.shared.WebLogger;

@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class UserHandler implements Serializable {

	private List<UserData> users;
	private UserData user = new UserData();
	
	private static final String VIEW_PAGE = "/modules/user/userview";
	
	@ManagedProperty(value="#{appHandler}")
	private AppHandler appHandler;

	public List<UserData> getUsers() {
		return users;
	}

	public void setUsers(List<UserData> users) {
		this.users = users;
	}

	public UserData getUser() {
		return user;
	}

	public void setUser(UserData user) {
		this.user = user;
	}

	public AppHandler getAppHandler() {
		return appHandler;
	}

	public void setAppHandler(AppHandler appHandler) {
		this.appHandler = appHandler;
	}
	
	public String processViewUsers() {
		WebLogger.info(appHandler.getUser(), "Viewing all users");
		users = UserDAO.getUsers();
		return VIEW_PAGE;
	}
	
	public String getView() {
		return VIEW_PAGE;
	}
	
	public void ajaxAddNewUser() {
		WebLogger.info(appHandler.getUser(), "Adding a new user");
		user = new UserData();
	}
	
	public void ajaxSaveUser() {
		WebLogger.info(appHandler.getUser(), "Saving user " + user.getFirstLast());
		if (user.getId() == -1) {
			// first, verify that the user id isn't already in use before creating new record
			if (UserDAO.doesUserExist(user.getUserId())) {
				appHandler.displayError("The entered username already exists.", null);
			} else {
				// name is unique, can save it
				String msg = "Adding user " + user.getFirstLast();
				WebLogger.debug(appHandler.getUser(), msg);
				UserDAO.addUser(user);
				UserDAO.updatePassword(user.getId(), "password");
			}
		} else {
			if (UserDAO.doesUserExist(user.getUserId(), user.getId())) {
				appHandler.displayError("The entered username exists.", null);
			} else {
				String msg = "Updating user " + user.getFirstLast();
				WebLogger.debug(appHandler.getUser(), msg);
				UserDAO.updateUser(user);
			}
		}			
	}
	
	public void ajaxUpdatePassword(String existingPassword, String newPassword, String newPasswordConfirm) {
		UserData user = UserDAO.authenticateUser(appHandler.getUser().getUserId(), existingPassword);
		boolean match = newPassword.equals(newPasswordConfirm);
		
		if (user == null) {
			appHandler.displayError("Invalid Password", "The password you entered is incorrect");
			return;
		}
		
		if (!match) {
			appHandler.displayError("Password Mismatch", "Passwords do not match");
			return;
		}
		
		WebLogger.debug("Changing password for logged in user");
		UserDAO.updatePassword(user.getId(), newPassword);
		appHandler.displayMessage("Password Changed", "Your password has been successfully updated");
	}
	
	public void ajaxUpdatePassword(int userId, String newPassword, String newPasswordConfirm) {
		UserData user = UserDAO.getUserById(userId);
		boolean match = newPassword.equals(newPasswordConfirm);
		
		if (user == null) {
			appHandler.displayError("User Not Found", "Unable to find the specified user");
			return;
		}
		
		if (!match) {
			appHandler.displayError("Password Mismatch", "Passwords do not match");
			return;
		}
		
		WebLogger.debug("Changing password for user " + user.getUserId());
		UserDAO.updatePassword(user.getId(), newPassword);
		appHandler.displayMessage("Password Changed", "User's password has been successfully updated");
	}
}
