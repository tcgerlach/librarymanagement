package edu.wgu.tgerlac.library.handler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import edu.wgu.tgerlac.library.dao.BookDAO;
import edu.wgu.tgerlac.library.dao.CheckoutDAO;
import edu.wgu.tgerlac.library.dao.CopyDAO;
import edu.wgu.tgerlac.library.dao.database.DBConnector;
import edu.wgu.tgerlac.library.model.BookData;
import edu.wgu.tgerlac.library.model.CopyData;
import edu.wgu.tgerlac.library.shared.FileFinder;
import edu.wgu.tgerlac.library.shared.WebLogger;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class BookHandler implements Serializable {
	
	/*
	 * NOTE: You can get the dewey decimal number at:
	 * http://classify.oclc.org/classify2/
	 */
	
	private List<BookData> books;
	private BookData selectedBook = new BookData();
	private List<CopyData> copies = new ArrayList<CopyData>();
	private CopyData selectedCopy = new CopyData();
	
	// search fields
	private String searchTitle;
	private String searchAuthor;
	private String searchISBN;
	private String searchSubject;
	
	private static final String VIEW_PAGE = "/modules/books/bookview";
	
	@ManagedProperty(value="#{appHandler}")
	private AppHandler appHandler;	
	
	public AppHandler getAppHandler() {
		return appHandler;
	}

	public void setAppHandler(AppHandler appHandler) {
		this.appHandler = appHandler;
	}
	
	public List<BookData> getBooks() {
		return books;
	}

	public void setBooks(List<BookData> books) {
		this.books = books;
	}

	public BookData getSelectedBook() {
		return selectedBook;
	}
	
	public void setSelectedBook(BookData book) {
		this.selectedBook = book;
		
		// load copies of book
		this.copies = CopyDAO.getCopiesByBook(book.getId());
	}

	public List<CopyData> getCopies() {
		return copies;
	}

	public void setCopies(List<CopyData> copies) {
		this.copies = copies;
	}
	
	public CopyData getSelectedCopy() {
		return selectedCopy;
	}

	public void setSelectedCopy(CopyData selectedCopy) {
		this.selectedCopy = selectedCopy;
	}

	public void addNewCopy() {
		WebLogger.info(appHandler.getUser(), "Adding copy of " + selectedBook.getTitle());
		this.copies.add(new CopyData(selectedBook.getId()));
	}

	public String getSearchTitle() {
		return searchTitle;
	}

	public void setSearchTitle(String searchTitle) {
		this.searchTitle = searchTitle;
	}

	public String getSearchAuthor() {
		return searchAuthor;
	}

	public void setSearchAuthor(String searchAuthor) {
		this.searchAuthor = searchAuthor;
	}

	public String getSearchISBN() {
		return searchISBN;
	}

	public void setSearchISBN(String searchISBN) {
		this.searchISBN = searchISBN;
	}

	public String getSearchSubject() {
		return searchSubject;
	}

	public void setSearchSubject(String searchSubject) {
		this.searchSubject = searchSubject;
	}

	public String processViewBooks() {
		WebLogger.info(appHandler.getUser(), "Viewing all books");
		books = BookDAO.getAllBooks();
		return VIEW_PAGE;
	}
	
	public void ajaxAddBook() {
		WebLogger.info(appHandler.getUser(), "Adding new book");
		selectedBook = new BookData();
		copies = new ArrayList<CopyData>();
	}
	
	public void ajaxRemoveCopy(CopyData copy) {
		// first, remove any checkouts on the copy
		// then, remove the copy
		CheckoutDAO.deleteCheckouts(copy.getId());
		CopyDAO.deleteCopy(copy.getId());
		copies.remove(copy);
	}
	
	public void ajaxSaveBook() {
		WebLogger.info(appHandler.getUser(), "Saving book");
		// first save book
		if (selectedBook.getId() > 0) {
			BookDAO.updateBook(selectedBook);
		} else {
			int id = BookDAO.insertBook(selectedBook);
			selectedBook.setId(id);
			books.add(selectedBook);
			Collections.sort(books);
		}
		
		// update number of copies for front end
		selectedBook.setCopies(copies.size());
		
		// then save copies
		for(CopyData copy : copies) {
			copy.setBookId(selectedBook.getId());
			if (copy.getId() > 0) {
				CopyDAO.updateCopy(copy);
			} else {
				int id = CopyDAO.insertCopy(copy);
				copy.setId(id);
			}
		}
	}
	
	public void ajaxFindBook() {
		WebLogger.info(appHandler.getUser(), "Searching for book by ISBN: " + searchISBN);
		this.setSelectedBook(BookDAO.getBookByISBN(searchISBN));
	}
	
	public void ajaxSearchBooks() {
		WebLogger.info("Searching for book");
		this.books = BookDAO.searchBooks(searchTitle, searchAuthor, searchISBN, searchSubject);
	}
	
	public void ajaxResetSearch() {
		this.searchAuthor = null;
		this.searchISBN = null;
		this.searchSubject = null;
		this.searchTitle = null;
		this.books = null;
	}
	
	// This class does NOT implement serializable 
	private transient StreamedContent pdfData;
	
	public StreamedContent processCheckoutReport() {
		return processReport("CheckedOut.jrxml", "checkout");
	}
	
	public StreamedContent processBookReport() {
		return processReport("Books.jrxml", "books");
	}
	
	public StreamedContent processReport(String file, String name) {
		WebLogger.info("Running report: " + file);
		try {
			
			// get report directory
			String resDir = FileFinder.getResourceDirectory() + "reports" + File.separator;
			
			// get report resource
			File reportFile = new File(resDir + file);
			FileInputStream fis = new FileInputStream(reportFile);
			
			// compile report
			JasperReport report = JasperCompileManager.compileReport(fis);
			
			// create parameter map
			// create parameter map
			Map<String, Object> params = new HashMap<String, Object>();			
			params.put("imageDir", resDir);		
			
			// populate report
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, params, DBConnector.getConnection());
			
			// stream content of PDF to user
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, os);
			InputStream is = new ByteArrayInputStream(os.toByteArray());
			pdfData = new DefaultStreamedContent(is,"application/pdf",name+".pdf");
		} catch (Exception e) {
			appHandler.displayError("Report Failed", "Failed to run report");
			WebLogger.error("Error creating report", e);
		}
		return pdfData;
	}
}
