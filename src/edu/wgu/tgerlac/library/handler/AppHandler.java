package edu.wgu.tgerlac.library.handler;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Stack;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import edu.wgu.tgerlac.library.model.UserData;
import edu.wgu.tgerlac.library.shared.BeanHelper;
import edu.wgu.tgerlac.library.shared.WebLogger;

/**
 * @author tcgerlach
 *
 * Base handler for sharing information common to multiple pages.
 */
@SuppressWarnings("serial")
@SessionScoped
@ManagedBean
public class AppHandler implements Serializable {
	
	private static final String TIME_FORMAT = "MM/dd/yyyy HH:mm";
	private static final String DATE_FORMAT = "MM/dd/yyyy";
	private static final String ROWS = "15";
	private static final String ROWS_PER_PAGE = "20,30,40";
	private static final String COPYRIGHT = "Copyright &copy;" + Calendar.getInstance().get(Calendar.YEAR) + ", Thomas C. Gerlach - All Rights Reserved";
	
	private Stack<String> lastPage = new Stack<String>();	
	private UserData user;
	
	private int currentTab;
	
	public AppHandler() {
		// NOTHING TO DO
	}
	
	/**
	 * Set the current logged in user.
	 * 
	 * @param user that is logged in
	 */
	public void setLoggedInUser(UserData user) {
		this.user = user;
	}
	
	/**
	 * Retrieve the logged in user information.
	 * 
	 * @return logged in user
	 */
	public UserData getUser() {
		return user;
	}
	
	/**
	 * Retrieve the copyright string for the main page.
	 * 
	 * @return copyright information
	 */
	public String getCopyright() {
		return COPYRIGHT;
	}
	
	/**
	 * Display an error message using growl.
	 * 
	 * @param summary
	 * @param detail
	 */
	public void displayError(String summary, String detail) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,summary,detail));
	}
	
	/**
	 * Display a message using growl.
	 * 
	 * @param summary
	 * @param detail
	 */
	public void displayMessage(String summary, String detail) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,summary,detail));
	}
	
	/** 
	 * Retrieve the string for the previously visited page.
	 * 
	 * @return previous page
	 */
	public String getLastPage() {
		String redirectTo;
		if (lastPage == null || lastPage.size() == 0) {
			redirectTo = goHome(); 
		} else {
			redirectTo = lastPage.pop();
		}
		
		WebLogger.info("Redirecting to " + redirectTo);
		return redirectTo;
	}	

	/**
	 * Set the last visited page in the stack.
	 * 
	 * @param lastPage page previously visited by the user
	 */
	public void setLastPage(String lastPage) {
		WebLogger.debug("Setting Last Page: "+ lastPage);
		this.lastPage.push(lastPage);
	}
	
	/**
	 * Retrieve the home location for the application.
	 * 
	 * @return home page
	 */
	public String goHome() {
		return BeanHelper.getBean(CheckoutHandler.class).processCheckoutBook();
	}	
	
	/** 
	 * Retrieve the system default time format.
	 * 
	 * @return Java time format
	 */
	public String getTimeFormat() {
		return TIME_FORMAT;
	}
	
	/** 
	 * Retrieve the system default date format.
	 * 
	 * @return Java date format
	 */
	public String getDateFormat() {
		return DATE_FORMAT;
	}
	
	/**
	 * Return the default number of rows for a table.
	 * 
	 * @return row count for tables
	 */
	public String getRows() {
		return ROWS;
	}
	
	/**
	 * Retrieve the template for the rows per page.
	 * This is used by PrimeFaces to create the drop down
	 * list for options.
	 * 
	 * @return page template
	 */
	public String getRowsPerPageTemplate() {
		return ROWS_PER_PAGE;
	}

	/**
	 * Return the current tab
	 * 
	 * @return current tab index
	 */
	public int getCurrentTab() {
		return currentTab;
	}

	/**
	 * Set the current tab
	 * 
	 * @param currentTab new current tab index
	 */
	public void setCurrentTab(int currentTab) {
		this.currentTab = currentTab;
	}
}
