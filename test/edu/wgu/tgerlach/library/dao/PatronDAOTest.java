package edu.wgu.tgerlach.library.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.wgu.tgerlac.library.dao.PatronDAO;
import edu.wgu.tgerlac.library.model.PatronData;

public class PatronDAOTest extends DAOTest {

	@Test
	public void testLoadAll() {
		int count = PatronDAO.getAllPatrons().size();
		assertTrue(count > 0);
	}
	
	@Test
	public void testLoadById() {
		PatronData patron = PatronDAO.getPatronById(1);
		assertEquals("Gerlach", patron.getLastName());
	}
}
