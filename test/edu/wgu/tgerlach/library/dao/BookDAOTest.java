package edu.wgu.tgerlach.library.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import edu.wgu.tgerlac.library.dao.BookDAO;
import edu.wgu.tgerlac.library.model.BookData;

public class BookDAOTest extends DAOTest {

	@Test
	public void testLoadAll() {
		int count = BookDAO.getAllBooks().size();
		assertTrue(count > 0);
	}
	
	@Test
	public void testLoadById() {
		BookData book = BookDAO.getBookById(1);
		assertEquals("computer", book.getSubject1());
	}
	
	@Test
	public void testLoadByISBN() {
		BookData book = BookDAO.getBookByISBN("9781259585494");
		assertEquals("computer", book.getSubject1());
	}
	
	@Test
	public void testSearch() {
		List<BookData> books = BookDAO.searchBooks("oracle", "", "", "");
		assertTrue(books.size() > 0);
	}
}
