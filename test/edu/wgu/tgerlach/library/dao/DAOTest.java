package edu.wgu.tgerlach.library.dao;

import org.junit.BeforeClass;

import edu.wgu.tgerlac.library.dao.database.DBConnector;

public abstract class DAOTest {
	
	/**
	 * NOTE: All tests are using data provided by the data.sql script. 
	 */
	
	@BeforeClass
	public static void setupConnector() {
		DBConnector.setUseJndiGetConnection(false);
	}	
}
