package edu.wgu.tgerlach.library.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import edu.wgu.tgerlac.library.dao.CopyDAO;
import edu.wgu.tgerlac.library.model.CopyData;

public class CopyDAOTest extends DAOTest {

	@Test
	public void testLoadById() {
		CopyData copy = CopyDAO.getCopyById(1);
		assertEquals("9781259585494", copy.getBarcode());
	}
	
	@Test
	public void testLoadByBarcode() {
		CopyData copy = CopyDAO.getCopyByBarcode("9781259585494");
		assertEquals("9781259585494", copy.getBarcode());
	}
	
	@Test
	public void testLoadByBook() {
		List<CopyData> copies = CopyDAO.getCopiesByBook(1);
		assertTrue(copies.size() > 0);
	}
}
