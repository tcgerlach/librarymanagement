$(document).ready(function(){
    reconstructWidth();
    
    // This blocks enter from submitting - necessary for scanner
    $('form').off('keypress.disableAutoSubmitOnEnter').on('keypress.disableAutoSubmitOnEnter', function(event) {
        if (event.which === $.ui.keyCode.ENTER && $(event.target).is('.scanner-tab')) {
            // prevent default behavior
        	event.preventDefault();
        	
        	if (typeof onScannerEnter === "function") {
        		onScannerEnter();
        	}
            
            // go to next item
            $('.tab-next').focus();
        }
        
        if (event.which === $.ui.keyCode.ENTER && $(event.target).is('.scanner-field')) {
            // prevent default behavior
        	event.preventDefault();
        }
    }); 
    
    $('.invisible-scanner').focus();
});

function reconstructWidth() {
    $(".ui-selectonemenu").each(function(){
        $(this).css({"width":"100%", "max-width":"160px", "min-width":"160px"});
    });	   
}

