-- NOTE: Salt is lib2019
INSERT INTO users (userid, password, lastname, firstname, admin) 
VALUES ('info@talixa.com', md5('lib2019password'), 'Admin', 'User', true);

INSERT INTO users (userid, password, lastname, firstname) 
VALUES ('tcgerlach@hotmail.com', md5('lib2019password'), 'Gerlach', 'Thomas');

INSERT INTO settings (name, value) VALUES ('LATE_FEE', '.25');
INSERT INTO settings (name, value) VALUES ('CHECKOUT_TIME', 14);

INSERT INTO books (id, title, author1, isbn, subject1, subject2, subject3, location, synopsis) 
VALUES (1, 'OCA Oracle Database SQL Exam Guide', 'Steve O''Hearn', '9781259585494', 'computer', 'sql', 'certification', '005.756',
	'The exclusive Oracle Press guide offers 100% coverage of all objectives of the Oracle Database SQL exam (1Z0-071), which will give you the Oracle Certified Associate certification.');
INSERT INTO copies (id, book_id, barcode) values (1, 1, '9781259585494');

INSERT INTO books (id, title, author1, isbn, subject1, subject2, subject3, location, synopsis)
VALUES (2, 'AWS Certified Solutions Architect Practice Tests', 'Brett McLaughlin', '9781119558439', 'computer', 'aws', 'certification', '004.678',
	'AWS Certified Solutions Architect Practice Tests: Associate SAA-C01 Exam is the ideal companion to the Sybex AW Certified SOlutions Architect Study Guide to provide a comprehensive, concise review.');
INSERT INTO copies (id, book_id, barcode) values (2, 2, '9781119558439');

INSERT INTO books (id, title, author1, author2, isbn, subject1, subject2, subject3, location, synopsis)
VALUES (3, 'AWS Certified Solutions Architect Study Guide', 'Ben Piper', 'David Clinton', '9781119504214', 'comptuer', 'aws', 'certification', '006.78',
	'The AWS Certified Solutions Architect Associate certification gives you a definite advantage in the competitive market for cloud specialists.');
INSERT INTO copies (id, book_id, barcode) values (3, 3, '9781119504214');

INSERT INTO books (id, title, author1, isbn, subject1, subject2, subject3, location, synopsis)
VALUES (4, 'The Art of R Programming', 'Norman Matloff', '9781593273842', 'computer', 'programming', 'r', '519.502',
	'R is the world''s most popular language for developing statistical software: Archaeologists use it to track the spread of ancient civilizations, drug companies use it to discover which medications are save and effective, and actuaries use it to assess financial risks and keep markets running smoothly.');
INSERT INTO copies (id, book_id, barcode) values (4, 4, '9781593273842');

INSERT INTO books (id, title, author1, isbn, subject1, subject2, subject3, location, synopsis)
VALUES (5, 'Machine Learning with Python for Everyone', 'Mark Fenner', '9780134845623', 'computer', 'programming', 'python', '005',
	'Machine Learning with Python for Everyone will help you master the processes, patterns, and strategies you need to build effective learning systems, even if you''re an absolute beginner');
INSERT INTO copies (id, book_id, barcode) values (5, 5, '9780134845623');

INSERT INTO patrons (id, lastname, firstname, barcode) values (1, 'Gerlach', 'Jolene', '5264633291');

INSERT INTO checkouts (id, copy_id, patron_id) values (1, 1, 1);
