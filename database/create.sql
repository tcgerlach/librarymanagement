use library;

drop table if exists checkouts;
drop table if exists copies;
drop table if exists books;
drop table if exists patrons;
drop table if exists users;
drop table if exists settings;

create table users (
	id int(11) NOT NULL AUTO_INCREMENT primary key,
	userid varchar(100) NOT NULL,
	password varchar(60) DEFAULT NULL,
	lastname varchar(45) DEFAULT NULL,
	firstname varchar(45) DEFAULT NULL,
	enabled BOOL NOT NULL DEFAULT TRUE,
	admin BOOL NOT NULL DEFAULT false,
	UNIQUE (userid)
);

create table books (
	id int primary key auto_increment,
	title varchar(256) not null,
	author1 varchar(64) not null,
	author2 varchar(64),
	subject1 varchar(32),
	subject2 varchar(32),
	subject3 varchar(32),
	isbn varchar(16),
	synopsis varchar(2048),
	location varchar(16)
);

create index book_isbn on books(isbn);
create index book_title on books(title);
create index book_author1 on books(author1);
create index book_author2 on books(author2);
create index book_subject1 on books(subject1);
create index book_subject2 on books(subject2);
create index book_subject3 on books(subject3);

create table copies (
	id int primary key auto_increment,
	book_id int references book(id),
	barcode varchar(16) unique
);

create table patrons (
	id int primary key auto_increment,
	lastname varchar(45) NOT NULL,
	firstname varchar(45) NOT NULL,
	address1 varchar(60),
	address2 varchar(60),
	city varchar(60),
	state char(2),
	zip char(5),
	phone char(10),
	barcode varchar(16) not null,
	active bool NOT NULL DEFAULT TRUE
);

CREATE INDEX patron_lname ON patrons(lastname);
CREATE INDEX patron_fname ON patrons(firstname);
CREATE INDEX patron_barcode on patrons(barcode);

create table checkouts (
	id int primary key AUTO_INCREMENT,
	copy_id int not null,
	patron_id int not null,
	outdate datetime default now(),
	indate datetime,
	foreign key checkout_patron(patron_id) references patrons(id),
	foreign key checkout_copy(copy_id) references copies(id)
);

create table settings (
	id int primary key auto_increment,
	name char(16) NOT NULL,
	value char(16) NOT null
);

create or replace view book_view as 
	select b.*, 
		count(c.id) as copies, 
		count(co.id) as checkedout 
	from books b
	left outer join copies c on c.book_id = b.id
	left outer join checkouts co on co.copy_id = c.id and co.indate is null
	group by b.id;
	
create or replace view checkout_view as 
	select *, 
	TIMESTAMPADD(DAY, (select value from settings where name = 'CHECKOUT_TIME'), c.outdate) as duedate
	from checkouts c;
	
create or replace view late_view as 
	select * from checkout_view where indate is null and now() > duedate;